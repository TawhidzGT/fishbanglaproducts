package com.example.fishbanglaproducts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fishbanglaproducts.Model.FishData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<FishData> fish_list;

    private boolean isLoadingAdded = false;

    RecyclerAdapter(Context context) {
        fish_list = new ArrayList<>();
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_loading, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.card_list, parent, false);
        viewHolder = new FishVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {

        FishData result = fish_list.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final FishVH fishVH = (FishVH) holder;
                fishVH.productName.setText(result.getProductName());
                fishVH.divisionName.setText(result.getDivisionName());
                fishVH.price.setText(result.getPrice());
                break;

            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return fish_list == null ? 0 : fish_list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == fish_list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    private void add(FishData r) {
        fish_list.add(r);
        notifyItemInserted(fish_list.size() - 1);
    }

    void addAll(List<FishData> moveResults) {
        for (FishData result : moveResults) {
            add(result);
        }
    }

    private void remove(FishData r) {
        int position = fish_list.indexOf(r);
        if (position > -1) {
            fish_list.remove(position);
            notifyItemRemoved(position);
        }
    }

    void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    boolean isEmpty() {
        return getItemCount() == 0;
    }


    void addLoadingFooter() {
        isLoadingAdded = true;
        add(new FishData());
    }

    void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = fish_list.size() - 1;
        FishData result = getItem(position);

        if (result != null) {
            fish_list.remove(position);
            notifyItemRemoved(position);
        }
    }

    private FishData getItem(int position) {
        return fish_list.get(position);
    }


    protected class FishVH extends RecyclerView.ViewHolder {
        private TextView productName;
        private TextView divisionName;
        private TextView price;

        FishVH(View itemView) {
            super(itemView);

            productName =  itemView.findViewById(R.id.productName);
            divisionName =  itemView.findViewById(R.id.divisionName);
            price =  itemView.findViewById(R.id.price);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
