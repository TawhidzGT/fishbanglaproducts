package com.example.fishbanglaproducts.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FishData {
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("upazillaName")
    @Expose
    private String upazillaName;
    @SerializedName("districtName")
    @Expose
    private String districtName;
    @SerializedName("unitSize")
    @Expose
    private String unitSize;
    @SerializedName("divisionName")
    @Expose
    private String divisionName;
    @SerializedName("appMenuTitle")
    @Expose
    private String appMenuTitle;
    @SerializedName("maxUnitQty")
    @Expose
    private String maxUnitQty;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("avgWeightUnit")
    @Expose
    private String avgWeightUnit;
    @SerializedName("unitType")
    @Expose
    private String unitType;
    @SerializedName("productPostType")
    @Expose
    private String productPostType;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("productPicture")
    @Expose
    private String productPicture;
    @SerializedName("avgWeight")
    @Expose
    private String avgWeight;
    @SerializedName("minUnitQty")
    @Expose
    private String minUnitQty;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUpazillaName() {
        return upazillaName;
    }

    public void setUpazillaName(String upazillaName) {
        this.upazillaName = upazillaName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getUnitSize() {
        return unitSize;
    }

    public void setUnitSize(String unitSize) {
        this.unitSize = unitSize;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getAppMenuTitle() {
        return appMenuTitle;
    }

    public void setAppMenuTitle(String appMenuTitle) {
        this.appMenuTitle = appMenuTitle;
    }

    public String getMaxUnitQty() {
        return maxUnitQty;
    }

    public void setMaxUnitQty(String maxUnitQty) {
        this.maxUnitQty = maxUnitQty;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAvgWeightUnit() {
        return avgWeightUnit;
    }

    public void setAvgWeightUnit(String avgWeightUnit) {
        this.avgWeightUnit = avgWeightUnit;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getProductPostType() {
        return productPostType;
    }

    public void setProductPostType(String productPostType) {
        this.productPostType = productPostType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProductPicture() {
        return productPicture;
    }

    public void setProductPicture(String productPicture) {
        this.productPicture = productPicture;
    }

    public String getAvgWeight() {
        return avgWeight;
    }

    public void setAvgWeight(String avgWeight) {
        this.avgWeight = avgWeight;
    }

    public String getMinUnitQty() {
        return minUnitQty;
    }

    public void setMinUnitQty(String minUnitQty) {
        this.minUnitQty = minUnitQty;
    }

}
