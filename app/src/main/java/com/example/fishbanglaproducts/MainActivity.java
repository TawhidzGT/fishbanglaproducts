package com.example.fishbanglaproducts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import com.example.fishbanglaproducts.Utils.PaginationScrollListener;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.fishbanglaproducts.Model.Fish;
import com.example.fishbanglaproducts.Model.FishData;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;

import Http.ApiClient;
import Http.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    private static final String TAG = "MainActivity";
    ProgressBar progressBar;

    private static final int PAGE_START = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int TOTAL_PAGES = 5;
    private int currentPage = PAGE_START;

    Fish Fish_List_Response;

    ArrayList<FishData> fish_list = new ArrayList<>();

    ApiInterface apiInterface = ApiClient.getBaseClient().create(ApiInterface.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        progressBar =  findViewById(R.id.main_progress);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(getApplicationContext());
        recyclerView.setAdapter(recyclerAdapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            public void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });



        LoadFishList();

    }

    private void LoadFishList()
    {
        Call<JsonElement> call = apiInterface.getImages("Basic R3RlY2g6R3RlY2hmYnYy",currentPage,5);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                try {
                    if (response.isSuccessful() && response.code() == 200) {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<Fish>() {
                        }.getType();
                        Fish_List_Response = gson.fromJson(response.body(), listType);
                        if (Fish_List_Response != null) {
                            fish_list.addAll(Fish_List_Response.getData());
                            progressBar.setVisibility(View.GONE);
                            recyclerAdapter.addAll(fish_list);
                            for(int i=0;i<fish_list.size();i++){
                                Log.d("check", fish_list.get(i).getProductId());
                            }
                            if (currentPage <= TOTAL_PAGES) recyclerAdapter.addLoadingFooter();
                            else isLastPage = true;
                        }
                    fish_list.clear();
                    }
                } catch (Exception ex) {
                    Log.d("check", Log.getStackTraceString(ex));
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                Log.d("check", t.toString());
            }
        });
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);
        Call<JsonElement> call = apiInterface.getImages("Basic R3RlY2g6R3RlY2hmYnYy",currentPage,5);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                try {
                    if (response.isSuccessful() && response.code() == 200) {
                        recyclerAdapter.removeLoadingFooter();
                        isLoading = false;
                        Gson gson = new Gson();
                        Type listType = new TypeToken<Fish>() {
                        }.getType();
                        Fish_List_Response = gson.fromJson(response.body(), listType);
                        if (Fish_List_Response != null) {
                            fish_list.addAll(Fish_List_Response.getData());
                            recyclerAdapter.addAll(fish_list);
                            for(int i=0;i<fish_list.size();i++){
                                Log.d("check", fish_list.get(i).getProductId());
                            }
                            if (currentPage != TOTAL_PAGES) recyclerAdapter.addLoadingFooter();
                            else isLastPage = true;
                        }
                        fish_list.clear();
                    }
                } catch (Exception ex) {
                    Log.d("check", Log.getStackTraceString(ex));
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                Log.d("check", t.toString());
            }
        });
    }

}
