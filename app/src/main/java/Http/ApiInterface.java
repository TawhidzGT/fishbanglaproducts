package Http;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("all")
    Call<JsonElement> getImages(@Header("Authorization") String apiKey,@Query("page") int pageIndex,
                                @Query("size") int pageSize);

}
